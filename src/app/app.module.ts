import { AppHeaderComponent } from "./app-header/app-header.component";
import { AppComponent } from "./app.component";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ROUTES } from "./app.routes";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LoginComponent } from './static/login/login.component';
@NgModule({
  declarations: [AppComponent, AppHeaderComponent, LoginComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
