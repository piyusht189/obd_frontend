import { ArenaComponent } from "./dashboard/components/arena/arena.component";
import { AppComponent } from "./app.component";

import { Routes } from "@angular/router";
import { RouteGuardService } from "./common/services/route-guard.service";
import { LoginComponent } from "./static/login/login.component";

export const ROUTES: Routes = [
  { path: "", component: LoginComponent },
  /*  { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistrationComponent },
    { path: 'requestDemo', component: RequestDemoComponent },
    { path: 'forgotPassword', component: ForgotPasswordComponent }, */
  /*  {
    path: "",
    component: ArenaComponent,
    canActivate: [RouteGuardService]
  }, */
  {
    path: "dashboard",
    loadChildren: "./dashboard/dashboard.module#DashboardModule"
  },
  /*  { path: 'preview', component: PreviewComponent },
    { path: 'display', component: DisplayComponent }, */
  { path: "**", redirectTo: "" }
];
