import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <div class="amcrest-main-container">
      <app-header class="amcrest-main-header" *ngIf="header"></app-header>
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent implements OnInit {
  header = true;
  constructor() {}

  ngOnInit() {}
}
