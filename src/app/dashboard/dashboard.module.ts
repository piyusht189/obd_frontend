import { ArenaComponent } from "./components/arena/arena.component";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      /* { path: '', component: ProjectEditorComponent }, */
      { path: "", component: ArenaComponent }
    ])
  ],
  declarations: [ArenaComponent],
  exports: [ArenaComponent]
})
export class DashboardModule {}
