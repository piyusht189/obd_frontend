# Amcrestgpstracker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Libraries

npm install jquery
npm install @types/jquery
npm install rxjs-compat
npm install @ngrx/store
npm install @ngrx/store-devtools
// npm install primeng
// npm install primeicons
npm install @ngrx/effects
npm install @ngrx/entity
npm install ngrx-store-freeze
// npm install normalizr

// npm install ngx-quill

npm install ngx-bootstrap
ng add @angular/material
npm install @angular/cdk
npm install angular-webstorage-service
// npm install ng2-pdf-viewer
npm install ngx-contextmenu
npm install ngrx-store-logger
npm install ng2-bootstrap-modal
npm install moment
// npm i angular-sortablejs
npm install file-saver
// npm install quill
// npm i sortablejs
npm install bootstrap
npm install mdbootstrap
// npm install angular2-draggable
npm install roboto-fontface
npm install font-awesome
// npm install ngx-file-drop
// npm install ngx-sortable

// ng add @angular/pwa
// npm install ngx-cacheable
// npm install ngx-auto-unsubscribe
// npm i @angular/material-moment-adapter
//npm install ngx-textarea-autosize
